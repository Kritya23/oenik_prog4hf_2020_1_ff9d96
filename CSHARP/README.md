# OENIK_PROG3_2019_2_FF9D96
Specie: crud
Classification: crud
Sub_class: crud
A crud generikus.
Egyébb logic:
BranchWriteout: kiírja a teljes leszármazási ágat az adott fajra.
SpeciesUnderSubClassCount: Megadja minden subclass alatt menyi a fajok száma.
FamilyCount: Megadja hány family