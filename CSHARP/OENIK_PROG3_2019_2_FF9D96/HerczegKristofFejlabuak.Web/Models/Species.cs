﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using HerczegKristofFejlabuak.Data;

namespace HerczegKristofFejlabuak.Web.Models
{
    public partial class Species
    {
        [Required]
        [Display(Name = "Specie's name")]
        public string species_name { get; set; }
        
        [Display(Name = "Discoverd")]
        public Nullable<short> felfedezes { get; set; }
       
        [Display(Name = "Appeared")]
        public string megjelenes { get; set; }
      
        [Display(Name = "Genus Name")]
        public string genus_name { get; set; }
       
        [Display(Name = "Legnagyobb Méret")]
        public string atlagos_meret { get; set; }
       
        [Display(Name = "Legnagyobb Méret")]
        public string legnagyobb_meret { get; set; }
      
        [Display(Name = "Cute?")]
        public Nullable<byte> cute { get; set; }
    
        [Display(Name = "Classification")]
        public virtual string classification { get; set; }


    }
}