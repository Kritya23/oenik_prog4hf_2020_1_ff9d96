﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HerczegKristofFejlabuak.Web.Models
{
    public class SpeciesViewModel
    {
        public Species  EditedSpecie { get; set; }
        public List<Species> ListOfSpecies { get; set; }

    }
}