﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace HerczegKristofFejlabuak.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg => 
            {

                cfg.CreateMap<HerczegKristofFejlabuak.Data.species, HerczegKristofFejlabuak.Web.Models.Species>().ForMember(dest => dest.classification, map => map.MapFrom(src => src.classification == null ? "": src.classification.order_));
                cfg.CreateMap<HerczegKristofFejlabuak.Data.species, HerczegKristofFejlabuak.Web.Models.Species>().ForMember(dest => dest.cute, map => map.MapFrom(src => src.cute));

                cfg.CreateMap<HerczegKristofFejlabuak.Data.species, HerczegKristofFejlabuak.Web.Models.Species>().ForMember(dest => dest.felfedezes, map => map.MapFrom(src => src.felfedezes));

                cfg.CreateMap<HerczegKristofFejlabuak.Data.species, HerczegKristofFejlabuak.Web.Models.Species>().ForMember(dest => dest.genus_name, map => map.MapFrom(src => src.genus_name));

                cfg.CreateMap<HerczegKristofFejlabuak.Data.species, HerczegKristofFejlabuak.Web.Models.Species>().ForMember(dest => dest.legnagyobb_meret, map => map.MapFrom(src => src.legnagyobb_meret));
                cfg.CreateMap<HerczegKristofFejlabuak.Data.species, HerczegKristofFejlabuak.Web.Models.Species>().ForMember(dest => dest.megjelenes, map => map.MapFrom(src => src.megjelenes));
                cfg.CreateMap<HerczegKristofFejlabuak.Data.species, HerczegKristofFejlabuak.Web.Models.Species>().ForMember(dest => dest.species_name, map => map.MapFrom(src => src.species_name));
                cfg.CreateMap<HerczegKristofFejlabuak.Data.species, HerczegKristofFejlabuak.Web.Models.Species>().ForMember(dest => dest.atlagos_meret, map => map.MapFrom(src => src.atlagos_meret));
            });
            return config.CreateMapper();
        }
    }
}