﻿using AutoMapper;
using HerczegKristofFejlabuak.Data;
using HerczegKristofFejlabuak.Logic;
using HerczegKristofFejlabuak.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HerczegKristofFejlabuak.Web.Controllers
{
    public class SpeciesApiController : ApiController
    {
        ILogic<species> logic;
        IMapper mapper;
        SpeciesApiController()
        {
            logic = new Logic<species>();
            mapper = MapperFactory.CreateMapper();
        }
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Species> GetAll()
        {
            var species = logic.GetAll();
            List<species> list = new List<species>();
            foreach (species item in species)
            {
                list.Add(item);
            }
            return mapper.Map<IList<Data.species>, IList<Models.Species>>(list);
        }
        //api/SpeciesApi/del/species_name
        [ActionName("del")]
        [HttpGet]
        public ApiResult DeleteOneSpecies(string nev)
        {
            ApiResult apiResult = new ApiResult() { OperationResult = true };
            try
            {
               

                logic.Delete(nev);
            }
            catch (Exception)
            {
                apiResult.OperationResult = false;


            }
            
            
            return apiResult;
        }

        //api/SpeciesApi/add + species
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneSpecies(Species Species)
        {
            ApiResult apiResult = new ApiResult() { OperationResult = true };
            try
            {
                species species = new species() { atlagos_meret = Species.atlagos_meret, cute = Species.cute, felfedezes = Species.felfedezes, genus_name = Species.genus_name, legnagyobb_meret = Species.legnagyobb_meret, megjelenes = Species.megjelenes, species_name = Species.species_name };

                logic.Create(species);
            }
            catch (Exception)
            {
                apiResult.OperationResult = false;




            }
            return apiResult;
        }

        // api/SpeciesApi/mod + species
        // nevet nem lehet megváltoztatni mert az az id
        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneSpecies(Species Species)
        {
            ApiResult apiResult = new ApiResult() { OperationResult = true };
            
            try
            {
                species species = new species() { atlagos_meret = Species.atlagos_meret, cute = Species.cute, felfedezes = Species.felfedezes, genus_name = Species.genus_name, legnagyobb_meret = Species.legnagyobb_meret, megjelenes = Species.megjelenes, species_name = Species.species_name };

                logic.Update(species, Species.species_name);
            }
            catch (Exception)
            {
                apiResult.OperationResult = false;




            }
            return apiResult;
        }
    }

    public class ApiResult
    {
        public bool OperationResult { get; set; }
    }
}
