﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using HerczegKristofFejlabuak.Data;
using HerczegKristofFejlabuak.Logic;
using HerczegKristofFejlabuak.Web.Models;


namespace HerczegKristofFejlabuak.Web.Controllers
{
    public class SpeciesController : Controller
    {
        ILogic<species> logic;
        IMapper mapper;
        SpeciesViewModel vm;

        public SpeciesController()
        {
            logic = new Logic<species>();
            mapper = MapperFactory.CreateMapper();

            vm = new SpeciesViewModel();
            vm.EditedSpecie = new Species();
            var Specie = logic.GetAll();
            List<species> specie = new List<species>();
            foreach (species item in Specie)
            {
                specie.Add(item);
            }
            vm.ListOfSpecies = mapper.Map<List<Data.species>, List<Models.Species>>(specie);
        }
        private Models.Species GetSpeciesModel(string name)
        {
            species species = logic.ReadOne(name);
            return mapper.Map<Data.species, Models.Species>(species);
        }
        // GET: Species
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("SpeciesIndex", vm);
        }

        // GET: Species/Details/5
        public ActionResult Details(string id)
        {
            return View("SpeciesDetails", GetSpeciesModel(id));
        }

        
        public ActionResult Remove(string id)
        {
            try
            {
                TempData["editResult"] = "Delete ok";
                logic.Delete(id);
                
            }
            catch (Exception)
            {

                TempData["editResult"] = "Delete Fail";

            }
            return RedirectToAction("Index");
        }
        public ActionResult Edit(string id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedSpecie = GetSpeciesModel(id);
            return View("SpeciesIndex", vm);
            
           
            
        }

        [HttpPost]
        public ActionResult Edit(Species species, string editAction)
        {
            if (ModelState.IsValid && species != null)
            {
                TempData["editResult"] = "Edit Ok";
                if (editAction == "AddNew")
                {
                    logic.Create(new Data.species() { species_name = species.species_name,
                     megjelenes = species.megjelenes,
                        atlagos_meret = species.atlagos_meret,
                        cute = species.cute,
                        felfedezes = species.felfedezes,
                        genus_name = species.genus_name,
                        classification = null,
                        legnagyobb_meret = species.legnagyobb_meret,
                    });
                }
                else
                {
                    try
                    {
                        logic.Update(new Data.species()
                        {
                            species_name = species.species_name,
                            megjelenes = species.megjelenes,
                            atlagos_meret = species.atlagos_meret,
                            cute = species.cute,
                            felfedezes = species.felfedezes,
                            genus_name = species.genus_name,
                            classification = null,
                            legnagyobb_meret = species.legnagyobb_meret,
                        }, species.species_name);
                    }
                    catch (Exception)
                    {

                        TempData["editResult"] = "Edit Fail";
                    }
                }
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedSpecie = species;
                return View("SpeciesIndex", vm);

            }
           
        }
    }
}
