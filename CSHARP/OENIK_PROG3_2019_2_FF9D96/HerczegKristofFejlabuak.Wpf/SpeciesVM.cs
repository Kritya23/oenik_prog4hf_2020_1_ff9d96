﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HerczegKristofFejlabuak.Wpf
{
    class SpeciesVM : ObservableObject
    {
        private string species_name;
        private Nullable<short> felfedezes;
        private string megjelenes;
        private string genus_name;
        private string atlagos_meret;
        private string legnagyobb_meret;
        private Nullable<byte> cute;
        private string classification;

        public string Species_name
        {
            get { return species_name; }
            set
            {
                Set(ref species_name, value);
            }
        }


        public Nullable<short> Felfedezes
        {
            get { return felfedezes; }
            set
            {
                Set(ref felfedezes, value);
            }
        }


        public string Megjelenes { get { return megjelenes; } 
            set{ Set(ref megjelenes, value);
            } }


        public string Genus_name
        {
            get { return genus_name; }
            set
            {
                Set(ref genus_name, value);
            }
        }


        public string Atlagos_meret
        {
            get { return atlagos_meret; }
            set
            {
                Set(ref atlagos_meret, value);
            }
        }


        public string Legnagyobb_meret
        {
            get { return legnagyobb_meret; }
            set
            {
                Set(ref legnagyobb_meret, value);
            }
        }


        public Nullable<byte> Cute
        {
            get { return cute; }
            set
            {
                Set(ref cute, value);
            }
        }

        public virtual string Classification
        {
            get { return classification; }
            set
            {
                Set(ref classification, value);
            }
        }
        public void CopyFrom(SpeciesVM other)
        {
            if (other == null) return;
            this.Species_name = other.Species_name;
            this.Atlagos_meret = other.Atlagos_meret;
            this.Legnagyobb_meret = other.legnagyobb_meret;
            this.Felfedezes = other.Felfedezes;
            this.Genus_name = other.Genus_name;
            this.Cute = other.Cute;
            this.Megjelenes = other.Megjelenes;

        }
    }
}
