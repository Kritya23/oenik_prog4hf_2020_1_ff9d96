﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HerczegKristofFejlabuak.Wpf
{
    class MainLogic
    {
        /// <summary>
        /// Nem müködik a del/modify a string id miatt, legalábbis ez a legjobb tippem
        /// </summary>
        string url = "http://localhost:49240/api/SpeciesApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "SpeciesResult");
        }
        public List<SpeciesVM> ApiGetSpecies()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<SpeciesVM>>(json);
            //SendMessage(true);
            return list;
        }
        public void ApiDelSpecies(SpeciesVM species)
        {
            bool success = false;
            if (species != null)
            {
                try
                {
                    string json = client.GetStringAsync(url + "del/" + species.Species_name).Result;
                    JObject obj = JObject.Parse(json);
                    success = true;
                }
                catch (Exception)
                {

                    success = false;
                }
               
            }
            SendMessage(success);
        }

        bool ApiEditSpecies(SpeciesVM species, bool isEditing)
        {
            if (species == null) return false;

            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();

            postData.Add(nameof(SpeciesVM.Species_name), species.Species_name.ToString());
            postData.Add(nameof(SpeciesVM.Felfedezes), species.Felfedezes.ToString());
            postData.Add(nameof(SpeciesVM.Genus_name), species.Genus_name.ToString());
            postData.Add(nameof(SpeciesVM.Legnagyobb_meret), species.Legnagyobb_meret.ToString());
            postData.Add(nameof(SpeciesVM.Megjelenes), species.Megjelenes.ToString());
            postData.Add(nameof(SpeciesVM.Atlagos_meret), species.Atlagos_meret.ToString());
            postData.Add(nameof(SpeciesVM.Cute), species.Cute.ToString());

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }
        public void EditSpecies(SpeciesVM species, Func<SpeciesVM, bool> editor)
        {
            SpeciesVM clone = new SpeciesVM();
            if (species != null)
            {
                clone.CopyFrom(species);
            }
            bool? success = editor?.Invoke(clone);

            if (success == true)
            {
                if (species != null)
                {
                    success = ApiEditSpecies(clone, true);
                }
                else
                {
                    success = ApiEditSpecies(clone, false);
                }
            }
            SendMessage(success == true);
        }

    }
}
