﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HerczegKristofFejlabuak.Wpf
{
    class MainVM  : ViewModelBase
    {
		private MainLogic logic;
		private SpeciesVM selectedSpecie;
		private ObservableCollection<SpeciesVM> allSpecies;

		public ObservableCollection<SpeciesVM> AllSpecies
		{
			get { return allSpecies; }
			set { Set(ref allSpecies, value); }
		}

		public SpeciesVM SelectedSpecies
		{
			get { return selectedSpecie; }
			set { Set(ref selectedSpecie, value); }
		}

		public ICommand AddCmd { get; private set; }
		public ICommand DelCmd { get; private set; }
		public ICommand ModCmd { get; private set; }
		public ICommand LoadCmd { get; private set; }

		public Func<SpeciesVM, bool> EditorFunc { get; set; }

		public MainVM()
		{
			logic = new MainLogic();

			DelCmd = new RelayCommand(() => logic.ApiDelSpecies(selectedSpecie));
			AddCmd = new RelayCommand(() => logic.EditSpecies(null, EditorFunc));
			ModCmd = new RelayCommand(() => logic.EditSpecies(selectedSpecie, EditorFunc));
			LoadCmd = new RelayCommand(() =>
			AllSpecies = new ObservableCollection<SpeciesVM>(logic.ApiGetSpecies()));
		}
	}
}
