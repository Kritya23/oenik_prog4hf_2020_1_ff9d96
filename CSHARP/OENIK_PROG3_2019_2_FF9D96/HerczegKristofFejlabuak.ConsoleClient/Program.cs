﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace HerczegKristofFejlabuak.ConsoleClient
{
    public class Species
    {

        public string species_name { get; set; }


        public Nullable<short> felfedezes { get; set; }


        public string megjelenes { get; set; }


        public string genus_name { get; set; }


        public string atlagos_meret { get; set; }


        public string legnagyobb_meret { get; set; }


        public Nullable<byte> cute { get; set; }

        public virtual string classification { get; set; }
        public override string ToString()
        {
            return $"Species name = {species_name} \t felfedezes = {felfedezes} megjelenes = {megjelenes} \t Genus name = {genus_name} \t Atlagos meret = {atlagos_meret} \t Legnagyobb meret = {legnagyobb_meret} ";
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("WAIT.......");
            Console.ReadLine();
            string url = "http://localhost:49240/api/SpeciesApi/";
            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Species>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
                Console.ReadLine();

                Dictionary<string, string> postData = new Dictionary<string, string>();

                string response;
                postData.Add(nameof(Species.species_name), "name");
                postData.Add(nameof(Species.felfedezes), "1800");
                postData.Add(nameof(Species.genus_name), "Gname");
                postData.Add(nameof(Species.legnagyobb_meret), "100");
                postData.Add(nameof(Species.megjelenes), "idk");
                postData.Add(nameof(Species.atlagos_meret), "40");
                postData.Add(nameof(Species.cute), "0");
                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: "+ json);
                Console.ReadLine();

                postData = new Dictionary<string, string>();

                string Species_name = JsonConvert.DeserializeObject<List<Species>>(json).Single(x => x.species_name == "N belauensis").species_name;
                postData.Add(nameof(Species.species_name), "N belauensis");
                postData.Add(nameof(Species.felfedezes), "1800");
                postData.Add(nameof(Species.genus_name), "siker");
                postData.Add(nameof(Species.legnagyobb_meret), "100");
                postData.Add(nameof(Species.megjelenes), "idk");
                postData.Add(nameof(Species.atlagos_meret), "40");
                postData.Add(nameof(Species.cute), "0");
                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                list = JsonConvert.DeserializeObject<List<Species>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
              
               
                Console.ReadLine();

                response = client.GetStringAsync(url + "del/"+Species_name).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                list = JsonConvert.DeserializeObject<List<Species>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
               
                Console.ReadLine();
            }  
        }
    }
}
